import json, os
import psycopg2
from psycopg2 import pool

host = os.environ['db_host']
port = os.environ['db_port']
db_user = os.environ['db_user']
pwd = os.environ['db_pass']
db = os.environ['db_db']

rs_pool = pool.SimpleConnectionPool(1,
                                    10,
                                    host = host,
                                    port = port,
                                    user = db_user,
                                    password = pwd,
                                    database = db)

import queries

def pool_get_users (event, context):
    result = None

    try:
        conn = rs_pool.getconn()
        cur = conn.cursor()

        query = queries.get_all_users
        print(query)
        cur.execute(query)
        result = cur.fetchall()

        cur.close()
        print(result)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        result = error
        rs_pool.putconn(conn)

    finally:
        if conn is not None:
            conn.close()
            print('Db connection closed')
            rs_pool.putconn(conn)
        res = {
            "statusCode": 200,
            "body": json.dumps(result)
        }
        return res

def conn_get_users (event, context):
    conn, result = None, None
    try:
        conn = psycopg2.connect(
            host = host,
            port = port,
            database = db,
            user = db_user,
            password = pwd
            )
        cur = conn.cursor()

        query = queries.get_all_users
        print(query)
        cur.execute(query)
        result = cur.fetchall()

        cur.close()
        print(result)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        result = error

    finally:
        if conn is not None:
            conn.close()
            print('Db connection closed')
        res = {
            "statusCode": 200,
            "body": json.dumps(result)
        }
        return res

def users_stream_handler (event, context):
    conn = None

    print(event)
    event_type = event['Records'][0]['eventName']

    if event_type == 'INSERT' or event_type == 'MODIFY':
        record = event['Records'][0]
        print('record')
        print(record)
        user = record['dynamodb']['NewImage']
        print('user')
        print(user)

        if not 'email' in user or not 'first_name' in user or not 'last_name' in user:
            print('Required field is missing')
            return

        try:
            conn = psycopg2.connect(
                host = host,
                port = port,
                database = db,
                user = db_user,
                password = pwd
                )
            cur = conn.cursor()
            query = queries.insert_user_query(user['email']['S'], user['first_name']['S'], user['last_name']['S'])
            print(query)
            cur.execute(query)
            result = cur.fetchall()
            print(result)
            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

        finally:
            if conn is not None:
                conn.close()
                print('Db connection closed')
            return
    elif event_type == 'REMOVE':
        print('REMOVE event')
        return
